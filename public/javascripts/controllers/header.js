(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('headerCtrl', headerCtrl);

    headerCtrl.$inject = ['$rootScope', '$state'];
    function headerCtrl($rootScope, $state) {

        $(document).ready(function () {

            // функция возвращает cookie с именем name, если есть, если нет, то undefined
            function getCookie(name) {
                var matches = document.cookie.match(new RegExp(
                    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                ));
                return matches ? decodeURIComponent(matches[1]) : undefined;
            }
            var cookiecook = getCookie("cookiecook"),
                cookiewin = document.getElementsByClassName('cookie_notice')[0];
// проверяем, есть ли у нас cookie, с которой мы не показываем окно и если нет, запускаем показ
            if (cookiecook != "no") {
                // показываем
                cookiewin.style.display="block";
                // закрываем по клику
                document.getElementById("cookie_close").addEventListener("click", function(){
                    cookiewin.style.display="none";
                    // записываем cookie на 1000 день, с которой мы не показываем окно
                    var date = new Date;
                    date.setDate(date.getDate() + 1000);
                    document.cookie = "cookiecook=no; path=/; expires=" + date.toUTCString();
                });
            }

        });

    }
}).call();