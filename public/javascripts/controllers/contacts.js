(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('ContactsCtrl', ContactsCtrl);

    ContactsCtrl.$inject = ['$location', '$rootScope', 'API', '$timeout'];
    function ContactsCtrl($location, $rootScope, API, $timeout) {

        var vm = this;

        vm.sendMessage = sendMessage;

        activate();

        function activate () {
            var loc = $location.$$url.replace('/','');
            $('.menu-a').removeClass('active');
            $('#' + loc).addClass('active');
            if(loc === 'contact'){
                $('#contact' ).addClass('active')
            }

            vm.nameErr = false;
            vm.emailErr = false;
            vm.messageErr = false;
            vm.waitingSend = false;
            vm.message = {};
            vm.statusSend = '';

        }

        function sendMessage(){
            if(!vm.message.name) vm.nameErr = true;
            if(!vm.message.email) vm.emailErr = true;
            if(!vm.message.text) vm.messageErr = true;
            if(vm.message.name && vm.message.email && vm.message.text){
                vm.waitingSend = true;
                return API.sendMess(vm.message).then(function (data) {
                    vm.waitingSend = false;
                    vm.statusSend = data.data;
                    $timeout(function(){
                        vm.statusSend = '';
                        vm.message = {};
                    },5000);
                });
            }
        }
    }
}).call();