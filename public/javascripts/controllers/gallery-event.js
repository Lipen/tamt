(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('GalleryEventCtrl', GalleryEventCtrl);

    GalleryEventCtrl.$inject = ['$location'];
    function GalleryEventCtrl($location) {

        var vm = this;

        activate();

        function activate () {
            var loc = $location.$$url.replace('/','');
            $('.menu-a').removeClass('active');
            $('#' + loc).addClass('active');
            if(loc === 'gallery-photo-events'){
                $('#gallery' ).addClass('active')
            }

            vm.allplays = [
                {name: 'День Т 2019', img:"../images/events/dayT19/DSC04580.jpg", id: '31'},
                {name: 'День Т 2016', img:"../images/events/dayT16/DSC09356.jpg", id: '32'},
                {name: 'День Т 2015', img:"../images/events/dayT15/DSC06667.jpg", id: '33'},
                {name: 'День Т 2010', img:"../images/events/dayT10/DSC07920.jpg", id: '34'},
                {name: 'закрытие спектакля "Все началось с какаду"', img:"../images/events/closekakadu/DSC04274.jpg", id: '27'},
                {name: 'открытие городской елки 2018-2019', img:"../images/events/elka/48424491.jpg", id: '22'},
                {name: 'Литературный пикник Таганрог', img:"../images/events/litparktag/DSC07586.jpg", id: '23'},
                {name: 'Литературный пикник Ростов', img:"../images/events/litparkrostv/DSCN4826.jpg", id: '24'},
                {name: 'исторический фестиваль "оборона таганрога 1855" (2017)', img:"../images/events/obortag2017/02.jpg", id: '25'},
                {name: 'исторический фестиваль "оборона таганрога 1855" (2018)', img:"../images/events/obortag2018/IMG_20181006_120009.jpg", id: '26'},
                {name: 'открытие скульптуры "толстый и тонкий"', img:"../images/events/толстыйтонкий/DSC08249.jpg", id: '21'},
                {name: '158 лет Чехову', img:"../images/events/158летЧехову/IMG_9436.jpg", id: '35'},
                {name: '100 лет революции', img:"../images/events/100летреволюции/IMG_8157.jpg", id: '36'}
                // ,{}
            ]
        }
    }
}).call();