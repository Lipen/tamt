(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('AboutCtrl', AboutCtrl);

    AboutCtrl.$inject = ['$location'];
    function AboutCtrl($location) {

        var vm = this;

        activate();

        function activate () {
            var loc = $location.$$url.replace('/','');
            $('.menu-a').removeClass('active');
            $('#' + loc).addClass('active');
            if(loc === 'about'){
                $('#about' ).addClass('active')
            }

            vm.aboutList = [
                {img: '../images/13звезда/DSC01146.JPG', name: '13я Звезда'},
                {img: '../images/бабаш/DSC09778.JPG', name: 'Баба Шанель'},
                {img: '../images/посвящение/IMG_3566.jpg', name: 'Посвящение'},
                {img: '../images/мертвыедуши/DSC04751.JPG', name: 'Мертвые души'}
            ]
        }


    }
}).call();