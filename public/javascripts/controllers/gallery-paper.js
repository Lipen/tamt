(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('GalleryPaperCtrl', GalleryPaperCtrl);

    GalleryPaperCtrl.$inject = ['$location'];
    function GalleryPaperCtrl($location) {

        var vm = this;

        activate();

        function activate () {
            var loc = $location.$$url.replace('/','');
            $('.menu-a').removeClass('active');
            $('#' + loc).addClass('active');
            if(loc === 'gallery-paper'){
                $('#gallery' ).addClass('active')
            }

            vm.allPapers = [
                // {name: 'Третий звонок', subName: "Выпуск 1", img:"../images/газета/выпуск1/wdtns.png", id: '1'}
                {name: 'Третий звонок', subName: "Выпуск 2", img:"../images/газета/выпуск2/tretiy_zvonok_2-1.jpg", id: '2'}
                // ,{name: 'Третий звонок. Выпуск 3', img:"../images/uthjcnhfn.png", id: '3'}
            ]
        }
    }
}).call();