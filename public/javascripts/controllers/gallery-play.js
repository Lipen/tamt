(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('GalleryPlayCtrl', GalleryPlayCtrl);

    GalleryPlayCtrl.$inject = ['$stateParams', '$window'];
    function GalleryPlayCtrl($stateParams, $window) {

        var vm = this;
        vm.goBack = goBack;

        activate();

        function activate () {
            vm.allplays = [
                {name: '13я ЗВЕЗДА', img:"images/13звезда/DSC00768.JPG", id: '1', allimages:
                    [
                        'images/13звезда/DSC00768.JPG',
                        'images/13звезда/DSC00831.JPG',
                        'images/13звезда/DSC01045.JPG',
                        'images/13звезда/DSC01079.JPG',
                        'images/13звезда/DSC01145.JPG',
                        'images/13звезда/DSC01146.JPG',
                        'images/13звезда/DSC01147.JPG'
                    ]
                }
                ,{name: 'БАБА CHANEL', img:"images/бабаш/DSC09819.JPG", id: '2',
                    allimages:
                        [
                            'images/бабаш/DSC00014.JPG',
                            'images/бабаш/DSC00078.JPG',
                            'images/бабаш/DSC00079.JPG',
                            'images/бабаш/DSC09778.JPG',
                            'images/бабаш/DSC09780.JPG',
                            'images/бабаш/DSC09791.JPG',
                            'images/бабаш/DSC09819.JPG',
                            'images/бабаш/DSC09867.JPG',
                            'images/бабаш/DSC09873.JPG',
                            'images/бабаш/DSC09878.JPG',
                            'images/бабаш/DSC09892.JPG'
                        ]
                }
                ,{name: 'ЦВЕТЫ ЗАПОЗДАЛЫЕ', img:"images/wdtns.png", id: '3',
                    allimages:
                        [
                            'images/цветы/DSC07524.jpg',
                            'images/цветы/DSC07531.jpg',
                            'images/цветы/DSC07550.jpg',
                            'images/цветы/DSC07579.jpg',
                            'images/цветы/DSC07642.jpg',
                            'images/цветы/DSC07648.jpg',
                            'images/цветы/DSC07704.jpg'
                        ]
                }
                ,{name: 'БРИТВА В КИСЕЛЕ', img:"images/бритва/DSC04946.JPG", id: '4',
                    allimages:
                        [
                            'images/бритва/DSC04946.JPG',
                            'images/бритва/DSC04955.JPG',
                            'images/бритва/DSC04970.JPG',
                            'images/бритва/DSC05001.jpg',
                            'images/бритва/DSC05002.JPG',
                            'images/бритва/DSC05004.JPG',
                            'images/бритва/DSC05007.jpg',
                            'images/бритва/DSC05014.jpg',
                            'images/бритва/DSC05015.JPG',
                            'images/бритва/DSC05016.jpg',
                            'images/бритва/DSC05018.jpg',
                            'images/бритва/DSC05026.jpg',
                            'images/бритва/DSC05027.jpg',
                            'images/бритва/DSC05031.jpg',
                            'images/бритва/DSC05032.JPG',
                            'images/бритва/DSC05037.JPG',
                            'images/бритва/DSC05060.jpg'
                        ]
                }
                ,{name: 'МЕРТВЫЕ ДУШИ', img:"images/мертвыедуши/DSC04813.JPG", id: '5',
                    allimages:
                        [
                            'images/мертвыедуши/DSC04719.JPG',
                            'images/мертвыедуши/DSC04734.JPG',
                            'images/мертвыедуши/DSC04751.JPG',
                            'images/мертвыедуши/DSC04778.JPG',
                            'images/мертвыедуши/DSC04788.JPG',
                            'images/мертвыедуши/DSC04813.JPG',
                            'images/мертвыедуши/5476.jpg',
                            'images/мертвыедуши/5517.jpg',
                            'images/мертвыедуши/5645.jpg',
                            'images/мертвыедуши/5419.jpg',
                            'images/мертвыедуши/5440.jpg',
                            'images/мертвыедуши/5447.jpg',
                            'images/мертвыедуши/5520.jpg',
                            'images/мертвыедуши/5642.jpg'
                        ]
                }
                ,{name: 'Дело в шляпе', img:"images/двш/DSC02983.JPG", id: '6',
                    allimages:
                        [
                            'images/двш/DSC02978.JPG',
                            'images/двш/DSC02980.JPG',
                            'images/двш/DSC02983.JPG',
                            'images/двш/DSC02993.JPG',
                            'images/двш/DSC03005.JPG',
                            'images/двш/DSC03012.JPG'
                        ]
                }
                ,{name: 'ВСЕ НАЧАЛОСЬ С КАКАДУ', img:"images/rfrfle.png", id: '7',
                    allimages:
                        [
                            'images/Какаду/DSC01815.JPG',
                            'images/Какаду/DSC01831.JPG',
                            'images/Какаду/DSC01837.JPG',
                            'images/Какаду/DSC01844.JPG',
                            'images/Какаду/DSC01867.JPG',
                            'images/Какаду/DSC01876.JPG',
                            'images/Какаду/DSC01881.JPG',
                            'images/Какаду/DSC01885.JPG'
                        ]
                }
                ,{name: 'ЗАБЫТЬ ГЕРОСТРАТА', img:"images/uthjcnhfn.png", id: '8',
                    allimages:
                        [
                            'images/герострат/GEROSTRAT_0005.JPG',
                            'images/герострат/GEROSTRAT_0016.JPG',
                            'images/герострат/GEROSTRAT_0018.JPG',
                            'images/герострат/GEROSTRAT_0023.JPG',
                            'images/герострат/GEROSTRAT_0025.JPG',
                            'images/герострат/GEROSTRAT_0034.jpg',
                            'images/герострат/GEROSTRAT_0039.JPG'
                        ]
                }
                ,{name: 'ПОСВЯЩЕНИЕ', img:"images/gjcdzotybt.png", id: '9',
                    allimages:
                        [
                            'images/посвящение/IMG_3391.jpg',
                            'images/посвящение/IMG_3403.jpg',
                            'images/посвящение/IMG_3521.jpg',
                            'images/посвящение/IMG_3528.jpg',
                            'images/посвящение/IMG_3566.jpg',
                            'images/посвящение/IMG_3617.jpg',
                            'images/посвящение/IMG_3668.jpg',
                            'images/посвящение/IMG_3690.jpg'
                        ]
                }
                ,{name: 'Как Зоя гусей кормила', img:"images/зоя/TCf0IQe85Q0.jpg", id: '10',
                    allimages:
                        [
                            'images/зоя/TCf0IQe85Q0.jpg',
                            'images/зоя/2b4mBf-bjec.jpg',
                            'images/зоя/4kBZ27JSzTA.jpg',
                            'images/зоя/_Q74SFDt8hM.jpg',
                            'images/зоя/ay7qu1WMKCU.jpg',
                            'images/зоя/BqbQJP2fsv4.jpg',
                            'images/зоя/MDrobZ_yNWk.jpg',
                            'images/зоя/n-xzpSaXSZs.jpg',
                            'images/зоя/R8B-59OEqro.jpg',
                            'images/зоя/VCjFG5-l9I0.jpg',
                            'images/зоя/VDeBmKqJENA.jpg'
                        ]
                }
                ,{name: 'Иллюзион', img:"images/иллюзион/P1170297.jpg", id: '11',
                    allimages:
                        [
                            'images/иллюзион/P1170297.jpg',
                            'images/иллюзион/P1170285.jpg',
                            'images/иллюзион/P1170290.jpg',
                            'images/иллюзион/P1170306.jpg',
                            'images/иллюзион/P1170309.jpg',
                            'images/иллюзион/P1170312.jpg',
                            'images/иллюзион/P1170318.jpg',
                            'images/иллюзион/P1170342.jpg',
                            'images/иллюзион/P1170435.jpg',
                            'images/иллюзион/P1170475.jpg'
                        ]
                }
                ,{name: 'Эти свободные бабочки', img:"images/бабочки/IMG0089.jpg", id: '12',
                    allimages:
                        [
                            'images/бабочки/IMG0089.jpg',
                            'images/бабочки/IMG0016.jpg',
                            'images/бабочки/IMG0042.jpg',
                            'images/бабочки/IMG0044.jpg',
                            'images/бабочки/IMG0060.jpg',
                            'images/бабочки/IMG0062.jpg',
                            'images/бабочки/IMG0086.jpg'
                        ]
                }
                ,{name: 'СКАЗКА НЕ ПО ПРАВИЛАМ', img:"../images/СНПП/DSC02961.jpg", id: '13',
                    allimages:
                        [
                            '../images/СНПП/DSC02961.jpg',
                            '../images/СНПП/DSC02882.jpg',
                            '../images/СНПП/DSC02906.jpg',
                            '../images/СНПП/DSC02910.jpg',
                            '../images/СНПП/DSC02922.jpg',
                            '../images/СНПП/DSC02923.jpg',
                            '../images/СНПП/DSC02942.jpg'
                        ]
                }
                ,{name: 'Африка', img:"../images/африка/DSC05474.jpg", id: '14',
                    allimages:
                        [
                            '../images/африка/DSC05474.jpg',
                            '../images/африка/DSC05275.jpg',
                            '../images/африка/DSC05325.jpg',
                            '../images/африка/DSC05335.jpg',
                            '../images/африка/DSC05387.jpg',
                            '../images/африка/DSC05395.jpg',
                            '../images/африка/DSC05400.jpg',
                            '../images/африка/DSC05404.jpg'
                        ]
                }
                ,{name: 'С другой стороны', img:"../images/сдругойстороны/IMG_2705.jpg", id: '15',
                    allimages:
                        [
                            '../images/сдругойстороны/IMG_2705.jpg',
                            '../images/сдругойстороны/IMG_2412.jpg',
                            '../images/сдругойстороны/IMG_2428.jpg',
                            '../images/сдругойстороны/IMG_2436.jpg',
                            '../images/сдругойстороны/IMG_2480.jpg',
                            '../images/сдругойстороны/IMG_2468.jpg',
                            '../images/сдругойстороны/IMG_2551.jpg',
                            '../images/сдругойстороны/IMG_2714.jpg'
                        ]
                }
                ,{name: 'Сказка о рыбаке и рыбке', img:"../images/рыба/IMG_3731.jpg", id: '16',
                    allimages:
                        [
                            '../images/рыба/IMG_3731.jpg',
                            '../images/рыба/IMG_3695.jpg',
                            '../images/рыба/IMG_3705.jpg',
                            '../images/рыба/IMG_3750.jpg',
                            '../images/рыба/IMG_3766.jpg',
                            '../images/рыба/IMG_3792.jpg'
                        ]
                }
                ,{name: 'кот в сапогах', img:"../images/кот/IMG_3817.jpg", id: '17',
                    allimages:
                        [
                            '../images/кот/IMG_3817.jpg',
                            '../images/кот/IMG_3826.jpg',
                            '../images/кот/IMG_3828.jpg',
                            '../images/кот/IMG_3832.jpg',
                            '../images/кот/IMG_3840.jpg'
                        ]
                }
                ,{name: 'Злодейка мирового масштаба', img:"../images/злодейка/DSC09599.jpg", id: '18',
                    allimages:
                        [
                            '../images/злодейка/DSC09599.jpg',
                            '../images/злодейка/DSC09598.jpg',
                            '../images/злодейка/DSC09651.jpg',
                            '../images/злодейка/DSC09679.jpg'
                        ]
                }
                ,{name: 'день рождения между небом и землей', img:"../images/ангелы/5.01.2011-0004.jpg", id: '19',
                    allimages:
                        [
                            '../images/ангелы/5.01.2011-0004.jpg',
                            '../images/ангелы/5.01.2011-0006.jpg',
                            '../images/ангелы/5.01.2011-0011.jpg',
                            '../images/ангелы/5.01.2011-0013.jpg',
                            '../images/ангелы/5.01.2011-0014.jpg',
                            '../images/ангелы/5.01.2011-0020.jpg',
                            '../images/ангелы/5.01.2011-0066.jpg'
                        ]
                }
                ,{name: 'лаборатория', img:"../images/events/lab/DSC03065.jpg", id: '20',
                    allimages: [
                        '../images/events/lab/DSC03065.jpg'
                        ,'../images/events/lab/DSC03073.jpg'
                        ,'../images/events/lab/DSC03221.jpg'
                        ,'../images/events/lab/DSC03224.jpg'
                        ,'../images/events/lab/DSC03226.jpg'
                        ,'../images/events/lab/DSC03228.jpg'
                        ,'../images/events/lab/DSC03229.jpg'
                        ,'../images/events/lab/DSC03230.jpg'
                        ,'../images/events/lab/DSC03231.jpg'
                        ,'../images/events/lab/DSC03234.jpg'
                        ,'../images/events/lab/DSC03240.jpg'
                        ,'../images/events/lab/DSC03247.jpg'
                        ,'../images/events/lab/IMG_4637.jpg'
                        ,'../images/events/lab/IMG_4639.jpg'
                        ,'../images/events/lab/IMG_4646.jpg'
                        ,'../images/events/lab/IMG_4650.jpg'
                        ,'../images/events/lab/IMG_4653.jpg'
                        ,'../images/events/lab/IMG_4655.jpg'
                        ,'../images/events/lab/IMG_4657.jpg'
                        ,'../images/events/lab/IMG_4658.jpg'
                        ,'../images/events/lab/IMG_4659.jpg'
                        ,'../images/events/lab/IMG_4660.jpg'
                        ,'../images/events/lab/IMG_4663.jpg'
                        ,'../images/events/lab/IMG_4667.jpg'
                        ,'../images/events/lab/IMG_4668.jpg'
                        ,'../images/events/lab/IMG_4669.jpg'
                        ,'../images/events/lab/IMG_4670.jpg'
                        ,'../images/events/lab/IMG_4677.jpg'
                        ,'../images/events/lab/IMG_4683.jpg'
                        ,'../images/events/lab/IMG_4684.jpg'
                        ,'../images/events/lab/IMG_4685.jpg'
                        ,'../images/events/lab/IMG_4686.jpg'
                        ,'../images/events/lab/IMG_4688.jpg'
                        ,'../images/events/lab/IMG_4689.jpg'
                        ,'../images/events/lab/IMG_4695.jpg'
                        ,'../images/events/lab/IMG_4710.jpg'
                        ,'../images/events/lab/IMG_4719.jpg'
                        ,'../images/events/lab/IMG_4722.jpg'
                        ,'../images/events/lab/IMG_4723.jpg'
                        ,'../images/events/lab/IMG_4733.jpg'
                        ,'../images/events/lab/IMG_4735.jpg'
                        ,'../images/events/lab/IMG_4740.jpg'
                        ,'../images/events/lab/IMG_4741.jpg'
                        ,'../images/events/lab/IMG_4743.jpg'
                    ]
                }
                ,{name: 'открытие скульптуры "толстый и тонкий"', img:"../images/events/толстыйтонкий/DSC08249.jpg", id: '21',
                    allimages: [
                        '../images/events/толстыйтонкий/DSC08249.jpg',
                        '../images/events/толстыйтонкий/DSC08240.jpg',
                        '../images/events/толстыйтонкий/DSC08271.jpg',
                        '../images/events/толстыйтонкий/DSC08274.jpg',
                        '../images/events/толстыйтонкий/DSC08279.jpg'
                    ]
                }
                ,{name: 'открытие городской елки 2018-2019', img:"../images/events/elka/48424491.jpg", id: '22',
                    allimages: [
                        '../images/events/elka/48424491.jpg',
                        '../images/events/elka/48378188o.jpg',
                        '../images/events/elka/48398413o.jpg',
                        '../images/events/elka/48398704.jpg',
                        '../images/events/elka/48412877o.jpg'
                    ]
                }
                ,{name: 'Литературный пикник Таганрог', img:"../images/events/litparktag/DSC07586.jpg", id: '23',
                    allimages: [
                        '../images/events/litparktag/DSC07586.jpg',
                        '../images/events/litparktag/DSC07568.jpg',
                        '../images/events/litparktag/DSC07593.jpg',
                        '../images/events/litparktag/DSC07654.jpg',
                        '../images/events/litparktag/DSC07701.jpg',
                        '../images/events/litparktag/DSC07725.jpg',
                        '../images/events/litparktag/sEP_OVU8u80.jpg',
                        '../images/events/litparktag/DSC07664.jpg'
                    ]
                }
                ,{name: 'Литературный пикник Ростов', img:"../images/events/litparkrostv/DSCN4826.jpg", id: '24',
                    allimages: [
                        '../images/events/litparkrostv/DSCN4826.jpg',
                        '../images/events/litparkrostv/DSCN4796.jpg',
                        '../images/events/litparkrostv/DSCN4804.jpg',
                        '../images/events/litparkrostv/DSCN4813.jpg',
                        '../images/events/litparkrostv/DSCN4870.jpg',
                        '../images/events/litparkrostv/DSCN4903.jpg',
                        '../images/events/litparkrostv/DSCN4842.jpg'
                    ]
                }
                ,{name: 'исторический фестиваль "оборона таганрога 1855" (2017)', img:"../images/events/obortag2017/02.jpg", id: '25',
                    allimages: [
                        '../images/events/obortag2017/02.jpg',
                        '../images/events/obortag2017/01.jpg',
                        '../images/events/obortag2017/7QOwrViTdPA.jpg',
                        '../images/events/obortag2017/bXD1c0imjiE.jpg',
                        '../images/events/obortag2017/DSC00355.jpg',
                        '../images/events/obortag2017/DSC00365а.jpg',
                        '../images/events/obortag2017/DSC00372.jpg'
                    ]
                }
                ,{name: 'исторический фестиваль "оборона таганрога 1855" (2018)', img:"../images/events/obortag2018/IMG_20181006_120009.jpg", id: '26',
                    allimages: [
                        '../images/events/obortag2018/IMG_20181006_120009.jpg',
                        '../images/events/obortag2018/IMG_20181006_102420.jpg',
                        '../images/events/obortag2018/IMG_20181006_102543.jpg',
                        '../images/events/obortag2018/IMG_20181006_102604.jpg'
                    ]
                }
                ,{name: 'закрытие спектакля "Все началось с какаду"', img:"../images/events/closekakadu/DSC04274.jpg", id: '27',
                    allimages: [
                        '../images/events/closekakadu/DSC04274.jpg',
                        '../images/events/closekakadu/DSC04192.jpg',
                        '../images/events/closekakadu/DSC04196.jpg',
                        '../images/events/closekakadu/DSC04206.jpg',
                        '../images/events/closekakadu/DSC04213.jpg',
                        '../images/events/closekakadu/DSC04253.jpg',
                        '../images/events/closekakadu/DSC04197.jpg'
                    ]
                }
                ,{name: '"утомленные солнцем 2" Н.Михалков', img:"../images/films/sun/С Михалковым.jpg", id: '28',
                    allimages: [
                        '../images/films/sun/С Михалковым.jpg',
                        '../images/films/sun/1.jpg',
                        '../images/films/sun/2.jpg',
                        '../images/films/sun/DSCN3823.jpg',
                        '../images/films/sun/DSCN3825.jpg'
                    ]
                }
                ,{name: '"Смотритель маяка" П.Дроздов', img:"../images/films/маяк/pCGAp9V9VQ0.jpg", id: '29',
                    allimages: [
                        '../images/films/маяк/pCGAp9V9VQ0.jpg',
                        '../images/films/маяк/21192107.jpg',
                        '../images/films/маяк/21246348.jpg',
                        '../images/films/маяк/21272923.jpg',
                        '../images/films/маяк/I38ieoqsRfo.jpg',
                        '../images/films/маяк/H3H8spllwwQ.jpg'
                    ]
                }
                ,{name: '"Гражданин щеколдин" Ю.Лаптев', img:"../images/films/щеколдин/qJJPmeprEeA.jpg", id: '30',
                    allimages: [
                        '../images/films/щеколдин/4eeCS6bT88o.jpg',
                        '../images/films/щеколдин/LC0nDGDzLyE.jpg',
                        '../images/films/щеколдин/qJJPmeprEeA.jpg'
                    ]
                }
                ,{name: 'День Т 2019', img:"../images/events/dayT19/DSC04580.jpg", id: '31',
                    allimages: [
                        '../images/events/dayT19/DSC04580.jpg',
                        '../images/events/dayT19/DSC04318.jpg',
                        '../images/events/dayT19/DSC04410.jpg',
                        '../images/events/dayT19/DSC04446.jpg',
                        '../images/events/dayT19/DSC04450.jpg',
                        '../images/events/dayT19/DSC04550.jpg',
                        '../images/events/dayT19/DSC04465.jpg'
                    ]
                }
                ,{name: 'День Т 2016', img:"../images/events/dayT16/DSC09356.jpg", id: '32',
                    allimages: [
                        '../images/events/dayT16/DSC09356.jpg',
                        '../images/events/dayT16/DSC09283.jpg',
                        '../images/events/dayT16/DSC09297.jpg',
                        '../images/events/dayT16/DSC09299.jpg',
                        '../images/events/dayT16/DSC09304.jpg',
                        '../images/events/dayT16/DSC09334.jpg',
                        '../images/events/dayT16/DSC09365.jpg',
                        '../images/events/dayT16/DSC09494.jpg',
                        '../images/events/dayT16/DSC09360.jpg'
                    ]
                }
                ,{name: 'День Т 2015', img:"../images/events/dayT15/DSC06667.jpg", id: '33',
                    allimages: [
                        '../images/events/dayT15/DSC06667.jpg',
                        '../images/events/dayT15/DSC06683.jpg',
                        '../images/events/dayT15/DSC06646.jpg',
                        '../images/events/dayT15/DSC06648.jpg'
                    ]
                }
                ,{name: 'День Т 2010', img:"../images/events/dayT10/DSC07920.jpg", id: '34',
                    allimages: [
                        '../images/events/dayT10/DSC07920.jpg',
                        '../images/events/dayT10/DSC07791.jpg',
                        '../images/events/dayT10/DSC07797.jpg',
                        '../images/events/dayT10/DSC07816.jpg',
                        '../images/events/dayT10/DSC07824.jpg',
                        '../images/events/dayT10/DSC07870.jpg',
                        '../images/events/dayT10/DSC07879.jpg',
                        '../images/events/dayT10/DSC07805.jpg'
                    ]
                }
                ,{name: '158 лет Чехову', img:"../images/events/158летЧехову/IMG_9436.jpg", id: '35',
                    allimages: [
                        '../images/events/158летЧехову/IMG_9436.jpg',
                        '../images/events/158летЧехову/IMG_9372.jpg',
                        '../images/events/158летЧехову/IMG_9438.jpg',
                        '../images/events/158летЧехову/IMG_9442.jpg',
                        '../images/events/158летЧехову/IMG_9446.jpg',
                        '../images/events/158летЧехову/IMG_9450.jpg'
                    ]
                }
                ,{name: '100 лет революции', img:"../images/events/100летреволюции/IMG_8157.jpg", id: '36',
                    allimages: [
                        '../images/events/100летреволюции/IMG_8157.jpg',
                        '../images/events/100летреволюции/IMG_8232.jpg',
                        '../images/events/100летреволюции/IMG_8294.jpg',
                        '../images/events/100летреволюции/IMG_8329.jpg',
                        '../images/events/100летреволюции/IMG_8340.jpg'
                    ]
                }
                ,{name: 'ГДЕ ЖИВЕТ ДРАКОН', img:"../images/дракон/IMG-20201217-WA0074.jpg", id: '37',
                    allimages: [
                        '../images/дракон/IMG-20201217-WA0074.jpg',
                        '../images/дракон/IMG-20201217-WA0048.jpg',
                        '../images/дракон/IMG-20201217-WA0049.jpg',
                        '../images/дракон/IMG-20201217-WA0068.jpg'
                    ]
                }


            ];

            vm.checkPlay = _.find(vm.allplays, function(item){
                return item.id === $stateParams.id;
            });
        }

        function goBack() {
            $window.history.back();
        }
    }
}).call();