(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('GalleryChildrenCtrl', GalleryChildrenCtrl);

    GalleryChildrenCtrl.$inject = ['$location'];
    function GalleryChildrenCtrl($location) {

        var vm = this;

        activate();

        function activate () {
            var loc = $location.$$url.replace('/','');
            $('.menu-a').removeClass('active');
            $('#' + loc).addClass('active');
            if(loc === 'gallery-photo-children'){
                $('#gallery' ).addClass('active')
            }

            vm.allplays = [
                {name: 'ГДЕ ЖИВЕТ ДРАКОН', img:"../images/дракон/IMG-20201217-WA0074.jpg", id: '37'}
                ,{name: 'СКАЗКА НЕ ПО ПРАВИЛАМ', img:"../images/СНПП/DSC02961.jpg", id: '13'}
                ,{name: 'Африка', img:"../images/африка/DSC05474.jpg", id: '14'}
                ,{name: 'С другой стороны', img:"../images/сдругойстороны/IMG_2705.jpg", id: '15'}
                ,{name: 'Сказка о рыбаке и рыбке', img:"../images/рыба/IMG_3731.jpg", id: '16'}
                ,{name: 'кот в сапогах', img:"../images/кот/IMG_3817.jpg", id: '17'}
                ,{name: 'Злодейка мирового масштаба', img:"../images/злодейка/DSC09599.jpg", id: '18'}
                ,{name: 'день рождения между небом и землей', img:"../images/ангелы/5.01.2011-0004.jpg", id: '19'}
            ]
        }
    }
}).call();