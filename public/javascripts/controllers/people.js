(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('PeopleCtrl', PeopleCtrl);

    PeopleCtrl.$inject = ['$location'];
    function PeopleCtrl($location) {

       var vm = this;

        activate();

        function activate () {
            var loc = $location.$$url.replace('/','');
            $('.menu-a').removeClass('active');
            $('#' + loc).addClass('active');
            if(loc === 'people'){
                $('#people' ).addClass('active');
            }

            vm.actors = [
                {name: 'Нонна Малыгина', mainImg: '../images/актеры/maligina.jpg', slug: 'малыгина'},
                {name: 'Екатерина Андрейчук', mainImg: '../images/актеры/SRG_0519.jpg', slug: 'андрейчук'},
                {name: 'Константин Илюхин', mainImg: '../images/актеры/SRG_0967.jpg', slug: 'илюхин'},
                {name: 'Александр Коваль', mainImg: '../images/актеры/SRG_0741.jpg', slug: 'коваль'},
                {name: 'Людмила Илюхина', mainImg: '../images/актеры/SRG_0776.jpg', slug: 'илюхина'},
                {name: 'Владимир Волжин', mainImg: '../images/актеры/SRG_0456.jpg', slug: 'волжин'},
                {name: 'Екатерина Власова', mainImg: '../images/актеры/SRG_1040.jpg', slug: 'власова'},
                {name: 'Иван Малахов', mainImg: '../images/актеры/sOt2jhdunA8.jpeg', slug: 'малахов'},
                {name: 'Светлана Липова', mainImg: '../images/актеры/2X_rmsTcMoM.jpg', slug: 'липова'},
                {name: 'Константин Решетило', mainImg: '../images/актеры/DRBWflSammo.jpg', slug: 'решетило'}
            ];

            vm.friends = [
                {name: "Фрейдлин Людмила Львовна", mainImg:"../images/friends/freindlin.jpg", slug: "фрейдлин"},
                {name: "Сергей Чехов", mainImg:"../images/friends/chehov.jpg", slug: "чехов"},
                {name: "Ханжаров Николай Мигдатович", mainImg:"../images/friends/hangarov.jpg", slug: "ханжаров"},
                {name: "Вера Сердечная", mainImg:"../images/friends/vera.jpg", slug: "сердечная"},
                {name: "Барвенко Виктория", mainImg:"../images/friends/barvenko.jpg", slug: "барвенко"},
                {name: "Антон Тимченко", mainImg:"../images/friends/5PRQtzpmgmg.jpg", slug: "тимченко"},
                {name: "Вера Ливеровская", mainImg:"../images/friends/liverovskaya.jpg", slug: "ливеровская"}
            ]
        }
    }
}).call();