(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('NewsCtrl', NewsCtrl);

    NewsCtrl.$inject = ['$location'];
    function NewsCtrl($location) {

        var vm = this;
        vm.chooseTab = chooseTab;
        vm.getIframeSrc = getIframeSrc;

        activate();

        function activate () {
            vm.selectedTab = 'web';
            var loc = $location.$$url.replace('/','');
            $('.menu-a').removeClass('active');
            if(loc === 'news'){
                $('#about' ).addClass('active');
            }

        }

        function getIframeSrc(src) {
            return src;
        }

        function chooseTab (type) {
            vm.selectedTab = type;
        }


        vm.paperSource = [
            {img:"../images/news/5.jpg", id: 32}
            ,{img:"../images/news/6.jpg", id: 31}
            ,{img:"../images/news/19.jpg", id: 30}
            ,{img:"../images/news/20.jpg", id: 29}
            ,{img:"../images/news/33_27429_24_marta_Sreda2.jpg", id: 28}
            ,{img:"../images/news/32_27428_19_marta_Pyatnitsa1.jpg", id: 27}
            ,{img:"../images/news/Театральный81.jpg", id: 26}
            ,{img:"../images/news/Театральный82.jpg", id: 25}
            ,{img:"../images/news/Театральный83.jpg", id: 24}
            ,{img:"../images/news/Театральный84.jpg", id: 23}
            ,{img:"../images/news/Театральный_№2_1.jpg", id: 22}
            ,{img:"../images/news/Театральный_№2_2.jpg", id: 21}
            ,{img:"../images/news/Театральный№1-8.jpg", id: 20}
            ,{img:"../images/news/Театральный№1-9.jpg", id: 19}
            ,{img:"../images/news/THEATER_11.jpg", id: 18}
            ,{img:"../images/news/Театральный№7:1.jpg", id: 17}
            ,{img:"../images/news/Театральный№7:2.jpg", id: 16}
            ,{img:"../images/news/Театральный№7:3.jpg", id: 15}
            ,{img:"../images/news/Театральный№7:4.jpg", id: 14}
            ,{img:"../images/news/nrDsZiJ59do.jpg", id: 13}
            ,{img:"../images/news/I3O1hnWtLvw.jpg", id: 12}
            ,{img:"../images/news/unnamed.jpg", id: 11}
            ,{img:"../images/news/TaMT_taganrogprav_BabaChanel.jpg", id: 10}
            ,{img:"../images/news/TaMT_LAMPA15_Britva.jpg", id: 9}
            ,{img:"../images/news/TaMT_LAMPA_20.jpg", id: 8}
            ,{img:"../images/news/TaMT_LAMPA_20:1.jpg", id: 7}
            ,{img:"../images/news/TaMT_NewVzglyad_Afrika.jpg", id: 6}
            ,{img:"../images/news/TaMT_LAMPA_BabaChanel.jpg", id: 5}
            ,{img:"../images/news/TaMT_taganrogprav_Kapustnik2014.jpg?", id: 4}
            ,{img:"../images/news/TaMT_TimeTaganroga_Cveti.jpg", id: 3}
            ,{img:"../images/news/TaMT_taganrogprav_Cveti.jpg", id: 2}
            ,{img:"../images/news/TaMT_taganrogKurier_Cveti.jpg", id: 1}
        ];

        vm.articleSource = [{img:"../images/news/отзывпогеростратустд.png", id: 1}
            ,{img:"../images/news/посвящениестд.jpg", id: 2}
        ];

        vm.videoSource = [
            {link: 'https://www.youtube.com/embed/nVdmYPu9wP4', id:18},
            {link: 'https://www.youtube.com/embed/HkPOd0oP524', id:17},
            {link: 'https://www.youtube.com/embed/64zYF9h5eHo', id:16},
            {link: 'https://www.youtube.com/embed/qDaSHx2CzZs', id:15},
            {link: 'https://www.youtube.com/embed/jdcL45h_-eE', id:14},
            {link: 'https://www.youtube.com/embed/3kZXEagiJQE ', id:13},
            {link: 'https://www.youtube.com/embed/Jy1vNl9T4kU', id:12},
            {link: 'https://www.youtube.com/embed/lDthVPWn73A', id:11},
            {link: 'https://www.youtube.com/embed/4QX11j85cPA', id:10},
            {link: 'https://www.youtube.com/embed/_FLa3Iy4QO4', id:9},
            {link: 'https://www.youtube.com/embed/p7yXMIitEdE', id:8},
            {link: 'https://www.youtube.com/embed/XjOh0pqFAqk', id:7},
            {link: 'https://www.youtube.com/embed/OXzVa6HpsSw', id:6},
            {link: 'https://www.youtube.com/embed/0jI-sm0aoJs', id:5},
            {link: 'https://www.youtube.com/embed/tjrgESemyFA', id:4},
            {link: 'https://www.youtube.com/embed/ZySi2KIkXts', id:3},
            {link: 'https://www.youtube.com/embed/Dw57pwJfI-s', id:2},
            {link: 'https://www.youtube.com/embed/4YRgumnqqO8', id:1}

        ];
    }
}).call();