(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['$location'];
    function HomeCtrl($location) {

        var vm = this;

        activate();

        function activate () {
            var loc = $location.$$url.replace('/','');
            $('.menu-a').removeClass('active');
            $('#' + loc).addClass('active');
            if(loc === 'home'){
                $('#home' ).addClass('active')
            }

            vm.slides = ['../images/13звезда/DSC00831.JPG'
                , '../images/13звезда/DSC01147.JPG'
                , '../images/DSC02963.JPG'
                , '../images/двш/DSC02993.JPG'
                , '../images/DSC04067.jpg'
                , '../images/цветы/DSC07648.jpg'
                , '../images/бабаш/DSC09819.JPG'
//                , '../images/GEROSTRAT_0034.jpg'
                , '../images/GEROSTRAT_0068.jpg'
                , '../images/IMG_3591.jpg'
                , '../images/рыба/IMG_3731.jpg'];

            // $(window).load(function(){
            //     $('.flexslider').flexslider({
            //         animation: "slide",
            //         slideshowSpeed: 7000,
            //         start: function(slider){
            //         }
            //     });
            // })
        }


    }
}).call();