(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('GalleryOldCtrl', GalleryOldCtrl);

    GalleryOldCtrl.$inject = ['$location'];
    function GalleryOldCtrl($location) {

        var vm = this;

        activate();

        function activate () {
            var loc = $location.$$url.replace('/','');
            $('.menu-a').removeClass('active');
            $('#' + loc).addClass('active');
            if(loc === 'gallery-photo-old'){
                $('#gallery' ).addClass('active')
            }

            vm.allplays = [
                {name: 'ЦВЕТЫ ЗАПОЗДАЛЫЕ', img:"../images/wdtns.png", id: '3'}
                ,{name: 'ВСЕ НАЧАЛОСЬ С КАКАДУ', img:"../images/rfrfle.png", id: '7'}
                ,{name: 'ЗАБЫТЬ ГЕРОСТРАТА', img:"../images/uthjcnhfn.png", id: '8'}
                ,{name: 'Иллюзион', img:"../images/иллюзион/P1170297.jpg", id: '11'}
                ,{name: 'Эти свободные бабочки', img:"../images/бабочки/IMG0089.jpg", id: '12'}
            ]
        }
    }
}).call();