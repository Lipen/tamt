(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('GalleryFilmsCtrl', GalleryFilmsCtrl);

    GalleryFilmsCtrl.$inject = ['$location'];
    function GalleryFilmsCtrl($location) {

        var vm = this;

        activate();

        function activate () {
            var loc = $location.$$url.replace('/','');
            $('.menu-a').removeClass('active');
            $('#' + loc).addClass('active');
            if(loc === 'gallery-films'){
                $('#gallery' ).addClass('active')
            }

            vm.allplays = [
                {name: '"утомленные солнцем 2" Н.Михалков', img:"../images/films/sun/С Михалковым.jpg", id: '28'}
                , {name: '"Смотритель маяка" П.Дроздов', img:"../images/films/маяк/pCGAp9V9VQ0.jpg", id: '29'}
                , {name: '"Гражданин щеколдин" Ю.Лаптев', img:"../images/films/щеколдин/qJJPmeprEeA.jpg", id: '30'}
                // ,{}
            ]
        }
    }
}).call();