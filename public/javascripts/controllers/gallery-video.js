(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('GalleryVideoCtrl', GalleryVideoCtrl);

    GalleryVideoCtrl.$inject = ['$location'];
    function GalleryVideoCtrl($location) {

        var vm = this;

        activate();

        function activate () {
            var loc = $location.$$url.replace('/','');
            $('.menu-a').removeClass('active');
            $('#' + loc).addClass('active');
            if(loc === 'gallery-video'){
                $('#gallery' ).addClass('active')
            }

            vm.allplays = [
                // {name: '13я ЗВЕЗДА', img:"../images/13звезда/DSC00768.JPG", id: '1'}
                // ,{name: 'БАБА CHANEL', img:"../images/бабаш/DSC09819.JPG", id: '2'}
                // ,{name: 'ЦВЕТЫ ЗАПОЗДАЛЫЕ', img:"../images/wdtns.png", id: '3'}
                // ,{name: 'БРИТВА В КИСЕЛЕ', img:"../images/бритва/DSC04946.JPG", id: '4'}
                // ,{name: 'МЕРТВЫЕ ДУШИ', img:"../images/мертвыедуши/DSC04813.JPG", id: '5'}
                // ,{name: 'Дело в шляпе', img:"../images/двш/DSC02983.JPG", id: '6'}
                // ,{name: 'ВСЕ НАЧАЛОСЬ С КАКАДУ', img:"../images/rfrfle.png", id: '7'}
                // ,{name: 'ЗАБЫТЬ ГЕРОСТРАТА', img:"../images/uthjcnhfn.png", id: '8'}
                // ,{name: 'ПОСВЯЩЕНИЕ', img:"../images/gjcdzotybt.png", id: '9'}
            ]
        }
    }
}).call();