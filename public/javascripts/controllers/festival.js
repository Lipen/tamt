(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('FestivalCtrl', FestivalCtrl);

    FestivalCtrl.$inject = ['$location'];
    function FestivalCtrl($location) {

       var vm = this;

        activate();

        function activate () {
            var loc = $location.$$url.replace('/','');
            $('.menu-a').removeClass('active');
            $('#' + loc).addClass('active');
            if(loc === 'festival'){
                $('#festival' ).addClass('active');
            }

            vm.allFestivals = [
                {name: 'I Международный театральный фестиваль-конкурс «Улица Чехова».', img:"images/репертуарки/festival2.jpeg", id: '5'},
                {name: 'Пятый Международный театральный фестиваль «Поговорим о любви…».', img:"images/festival/1621113873678.jpg", id: '1'},
                {name: 'Фестиваль театров малых городов России', img:"images/festival/1000х500_af-01.jpg", id: '2'},
                {name: 'Третий Международный театральный фестиваль «Театральная гавань».', img:"images/festival/1621113873684.jpg", id: '3'},
                {name: 'Двадцать пятый Международный моложедный театральный фестиваль «Русская класика».', img:"images/festival/rucl.jpg", id: '4'}
            ]
        }
    }
}).call();