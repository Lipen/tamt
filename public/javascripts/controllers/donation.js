(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('DonationCtrl', DonationCtrl);

    DonationCtrl.$inject = ['amount', '$stateParams', 'API', '$state'];
    function DonationCtrl(amount, $stateParams, API, $state) {

        var vm = this;
        vm.y_call_back = y_call_back;
        vm.sendThanks = sendThanks;
        vm.sendDonationRequest = sendDonationRequest;
        vm.runWidget = runWidget;
        vm.userInfo = {amount: 0};
        vm.amount = amount;

        activate();

        function activate () {
            $('#donation' ).addClass('active');
            vm.nameErr = false;
            vm.emailErr = false;
            vm.amountErr = false;
            vm.yandexErr = false;
            console.log('%%%', $stateParams);
            if($stateParams.email) {
                var dlgElem = angular.element("#myModal2");
                if (dlgElem) {
                    dlgElem.modal("show");
                }
            }
        }

        function y_call_back(error){
            console.log('some error', error);
        }

        function sendThanks(){
            if($stateParams.email){
                return API.sendThank($stateParams.email).then(function () {
                    $stateParams.email = "";
                    $stateParams.name = "";
                    $state.go('donation',$stateParams, { reload: true });

                });
            }
        }

        function runWidget(token, email, name){
            console.log('token', token);
            const checkout = new window.YandexCheckout({
                confirmation_token: token,
                return_url: 'https://tagteatr.ru/donation?email='+email+'&name='+name,
                error_callback: vm.y_call_back
            });
            checkout.render('yandex-payment-form');
            var dlgElem = angular.element("#myModal");
            if (dlgElem) {
                dlgElem.modal("show");
            }
        }

        function sendDonationRequest(){
            if(vm.amount !== "0"){
                vm.userInfo.amount = +vm.amount;
            }
            if(!vm.userInfo.name) vm.nameErr = true;
            if(!vm.userInfo.email) vm.emailErr = true;
            if(!vm.userInfo.amount || vm.userInfo.amount === 0) vm.amountErr = true;
            if(vm.userInfo.name && vm.userInfo.email && !vm.amountErr){
                return API.sendPayment(vm.userInfo).then(function (data) {
                    if(data&& data.data && data.data.confirmation_token){
                        runWidget(data.data.confirmation_token, vm.userInfo.email, vm.userInfo.name)
                    } else {
                        vm.yandexErr = true;
                    }
                });
            }
        }


    }
}).call();