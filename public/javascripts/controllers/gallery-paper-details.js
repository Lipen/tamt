(function () {
    'use strict';
    angular
        .module('tamt.controllers')
        .controller('GalleryPaperDetailsCtrl', GalleryPaperDetailsCtrl);

    GalleryPaperDetailsCtrl.$inject = ['$stateParams', '$window'];
    function GalleryPaperDetailsCtrl($stateParams, $window) {

        var vm = this;
        vm.goBack = goBack;

        activate();

        function activate () {
            vm.allPapers = [
                {name: 'Третий звонок. Выпуск 1', img:"images/газета/выпуск1/wdtns.png", id: '1',
                    allimages: [
                        ''
                    ]
                }
                ,{name: 'Третий звонок. Выпуск 2', img:"images/газета/выпуск2/tretiy_zvonok_2-1.jpg", id: '2',
                    allimages: [
                        'images/газета/выпуск2/tretiy_zvonok_2-1.jpg',
                        'images/газета/выпуск2/tretiy_zvonok_2-2.jpg',
                        'images/газета/выпуск2/tretiy_zvonok_2-3.jpg',
                        'images/газета/выпуск2/tretiy_zvonok_2-4.jpg',
                        'images/газета/выпуск2/tretiy_zvonok_2-5.jpg',
                        'images/газета/выпуск2/tretiy_zvonok_2-6.jpg',
                        'images/газета/выпуск2/tretiy_zvonok_2-7.jpg',
                        'images/газета/выпуск2/tretiy_zvonok_2-8.jpg'
                    ]
                }
                // ,{name: 'Третий звонок. Выпуск 3', img:"../images/uthjcnhfn.png", id: '3'}
            ];

            vm.checkPaper = _.find(vm.allPapers, function(item){
                return item.id === $stateParams.id;
            });
        }

        function goBack() {
            $window.history.back();
        }
    }
}).call();