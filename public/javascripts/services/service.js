(function () {
    'use strict';
    angular
        .module('tamt.services')
        .factory('API', API);

    API.$inject = ['$http'];
    function API($http) {

        return {
            sendMess: sendMess,
            sendThank: sendThank,
            sendPayment: sendPayment
        };

        function sendMess(mess) {
            return $http({
                method: 'POST', url: '/user/contact', data: mess
            })
                .then(getComplete)
                .catch(getFailed);

        }

        function sendPayment(data) {
            return $http({
                method: 'POST', url: '/user/yandex-payment', data: data
            })
                .then(getComplete)
                .catch(getFailed);

        }

        function sendThank(email) {
            return $http({
                method: 'POST', url: '/user/thanks', data: {email: email}
            })
                .then(getComplete)
                .catch(getFailed);

        }
    }

    function getComplete(response) {
        return response
    }

    function getFailed(error) {
        return error
    }

}).call();

