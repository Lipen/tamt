(function () {
  'use strict';

  angular.module('tamt')
    .config(['$stateProvider', '$urlRouterProvider', '$sceDelegateProvider', '$locationProvider',
      function ($stateProvider, $urlRouterProvider, $sceDelegateProvider, $locationProvider) {
        $sceDelegateProvider.resourceUrlWhitelist([
            'self',
            '*://www.youtube.com/**'
        ]);
        $stateProvider
          .state('home', {
            url: '/home',
              views: {
                '@': {
                  templateUrl: 'partials/home.html',
                  controller: 'HomeCtrl as tamt'
                }
            }
          })
          .state('about', {
            url: '/about',
              views: {
                '@': {
                  templateUrl: 'partials/about.html',
                  controller: 'AboutCtrl as tamt'
                }
            }
          })
          .state('plays', {
            url: '/plays',
            views: {
              '@': {
                templateUrl: '/partials/plays.html',
                controller: 'PlaysCtrl as tamt'
              }
            }
          })
          .state('plays-children', {
            url: '/plays-children',
            views: {
              '@': {
                templateUrl: '/partials/plays-children.html',
                controller: 'PlaysChildrenCtrl as tamt'
              }
            }
          })
          .state('plays-old', {
            url: '/plays-old',
            views: {
              '@': {
                templateUrl: '/partials/plays-old.html',
                controller: 'PlaysOldCtrl as tamt'
              }
            }
          })
          .state('novel', {
            url: '/novel',
            views: {
              '@': {
                templateUrl: '/partials/news.html',
                controller: 'NewsCtrl as tamt'
              }
            }
          })
          .state('festival', {
            url: '/festival',
            views: {
              '@': {
                templateUrl: '/partials/festival.html',
                controller: 'FestivalCtrl as tamt'
              }
            }
          })
          .state('festival info', {
            url: '/festival/:id',
            views: {
              '@': {
                templateUrl: '/partials/festival-info.html',
                controller: 'FestivalInfoCtrl as tamt'
              }
            }
          })
          .state('donation', {
            url: '/donation?email=&name',
            views: {
              '@': {
                templateUrl: '/partials/donation.html',
                controller: 'DonationCtrl as tamt'
              }
            },
            resolve: {
                amount: function () {
                   return "500";
                }
            }
          })
          .state('gallery-photo', {
            url: '/gallery-photo',
            views: {
              '@': {
                templateUrl: '/partials/gallery-photo.html',
                controller: 'GalleryCtrl as tamt'
              }
            }
          })
          .state('gallery-photo-children', {
            url: '/gallery-photo-children',
            views: {
              '@': {
                templateUrl: '/partials/gallery-photo.html',
                controller: 'GalleryChildrenCtrl as tamt'
              }
            }
          })
          .state('gallery-photo-old', {
            url: '/gallery-photo-old',
            views: {
              '@': {
                templateUrl: '/partials/gallery-photo.html',
                controller: 'GalleryOldCtrl as tamt'
              }
            }
          })
          .state('gallery-photo-events', {
            url: '/gallery-photo-events',
            views: {
              '@': {
                templateUrl: '/partials/gallery-photo.html',
                controller: 'GalleryEventCtrl as tamt'
              }
            }
          })
          .state('gallery-films', {
            url: '/gallery-films',
            views: {
              '@': {
                templateUrl: '/partials/gallery-photo.html',
                controller: 'GalleryFilmsCtrl as tamt'
              }
            }
          })
          .state('gallery-video', {
            url: '/gallery-video',
            views: {
              '@': {
                templateUrl: '/partials/gallery-video.html',
                controller: 'GalleryVideoCtrl as tamt'
              }
            }
          })
          .state('gallery-paper', {
            url: '/gallery-paper',
            views: {
              '@': {
                templateUrl: '/partials/gallery-paper.html',
                controller: 'GalleryPaperCtrl as tamt'
              }
            }
          })
          .state('gallery-paper details', {
            url: '/gallery-paper/:id',
            views: {
              '@': {
                templateUrl: '/partials/gallery-paper-details.html',
                controller: 'GalleryPaperDetailsCtrl as tamt'
              }
            }
          })
          .state('gallery play', {
            url: '/gallery/:id',
            views: {
              '@': {
                templateUrl: '/partials/gallery-play.html',
                controller: 'GalleryPlayCtrl as tamt'
              }
            }
          })
          .state('contacts', {
            url: '/contacts',
            views: {
              '@': {
                templateUrl: '/partials/contacts.html',
                controller: 'ContactsCtrl as tamt'
              }
            }
          })
          .state('people', {
            url: '/people',
            views: {
              '@': {
                templateUrl: '/partials/people.html',
                controller: 'PeopleCtrl as tamt'
              }
            }
          })
          .state('people details', {
            url: '/people/:id',
            views: {
              '@': {
                templateUrl: '/partials/peopleDetails.html',
                controller: 'PeopleDetailsCtrl as tamt'
              }
            }
          })
          .state('news', {
            url: '/news',
            views: {
              '@': {
                templateUrl: '/partials/news.html',
                controller: 'NewsCtrl as tamt'
              }
            }
          });

        $urlRouterProvider.otherwise('home');
        if(window.history && window.history.pushState){
            $locationProvider.html5Mode(true);
        }
      }]);

}).call();
