(function () {
    'use strict';
    angular
        .module('tamt.directives')
        .directive('slideToggle', slideToggle);

    slideToggle.$inject = [];
    function slideToggle() {
        return {
            restrict: 'A',
            scope:{
                isOpen: "=slideToggle"
            },
            link: function(scope, element, attr) {
                var slideDuration = parseInt(attr.slideToggleDuration, 10) || 200;
                scope.$watch('isOpen', function(newVal,oldVal){
                    if(newVal !== oldVal){
                        $('#' + element[0].id).slideToggle(slideDuration);
                    }
                });
            }
        };
    }
}).call();
