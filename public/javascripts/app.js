(function () {
    'use strict';
    angular.module('tamt', [
        'ui.router',
        'tamt.controllers',
        'tamt.services',
        'tamt.directives',
        // 'angular-flexslider',
        'angularFileUpload'
    ]);
}).call();