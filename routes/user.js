var fs = require('fs')
    , request = require('request')
    , uuid = require('uuid')
    , sgMail = require('@sendgrid/mail');


exports.boot = function (app, config) {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);

    function sendEmail(msg, callback) {
        sgMail
            .send(msg)
            .then(function(){
                callback(null, 'success')
            }, function(error){
                console.log('^^^', error);
                callback(error, 'error')
            });
    }

    app.post('/contact', function (req, res) {
        if (req.body) {

            const msg = {
                to: config.mail_send,
                from: 'support@tagteatr.ru',
                subject: req.body.theme || '',
                html: "Пользователь: <b>" + req.body.name + "</b><br>" + "Email: <b>" + req.body.email + "</b><br>" + "Сообщение: <b>" + req.body.text + "</b>"
            };
            sendEmail(msg, function (err, result) {
                if(err){
                    res.send('Ошибка при отправке сообщения');
                } else{
                    res.send('Ваше сообщение отправлено!');
                }
            });
        } else {
            res.send('Ошибка при отправке сообщения');
        }
    });

    app.post('/thanks', function (req, res) {
        if (req.body) {
            var name = 'зритель';

            const msg = {
                to: req.body.email,
                from: 'support@tagteatr.ru',
                subject: 'Благодарим за помощь!',
                html: "<html lang=\"en\"><head>\n" +
                "  <meta charset=\"UTF-8\">\n" +
                "  <title>TaKT Donation</title>\n" +
                "  \n" +
                "\n" +
                "</head>\n" +
                "<body style=\"margin: 0; padding: 0;\">\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "    <title>Thanks</title>\n" +
                "    <style type=\"text/css\">\n" +
                "        html {\n" +
                "            -webkit-text-size-adjust: none;\n" +
                "            -ms-text-size-adjust: none;\n" +
                "        }\n" +
                "\n" +
                "        @media only screen and (min-device-width: 660px) {\n" +
                "            .table660 {\n" +
                "                width: 660px !important;\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        @media only screen and (max-device-width: 660px), only screen and (max-width: 660px) {\n" +
                "            .table660 {\n" +
                "                width: 100% !important;\n" +
                "            }\n" +
                "\n" +
                "            .mob-radius-right {\n" +
                "                border-radius: 0 3px 3px 0;\n" +
                "            }\n" +
                "\n" +
                "            .mob-radius-left {\n" +
                "                border-radius: 3px 0 0 3px;\n" +
                "                border-left: 1px solid #ebebeb !important;\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        .table660 {\n" +
                "            width: 660px;\n" +
                "        }\n" +
                "    </style>\n" +
                "\n" +
                "\n" +
                "\n" +
                "    <span class=\"preheader\" style=\"display: none !important; visibility: hidden; opacity: 0; color: #fff; height: 0; width: 0; font-size: 1px;\"></span>\n" +
                "\n" +
                "    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"font-size: 1px; line-height: normal;\">\n" +
                "        <tbody><tr>\n" +
                "            <td align=\"center\" bgcolor=\"#ffffff\" style=\"background-color: #ffffff;\">\n" +
                "                <!--[if (gte mso 9)|(IE)]>\n" +
                "                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"660\"><tr><td>\n" +
                "                <![endif]-->\n" +
                "                <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" border=\"0\" class=\"table660\" style=\"max-width: 660px; min-width: 320px; width: 100%;\">\n" +
                "                    <tbody><tr>\n" +
                "                        <td align=\"center\" valign=\"top\">\n" +
                "                            \n" +
                "                            <a href=\"tagteatr.ru\" target=\"_blank\"><img src=\"https://tagteatr.ru/images/favicon/ms-icon-310x310.png\" height=\"250\" width=\"auto\" border=\"0\" alt=\"\" style=\"display: inline-block\"></a>\n" +
                "                            \n" +
                "                            <div style=\"height: 1px; line-height: 1px; font-size: 1px; border-top-width: 1px; border-top-style: solid; border-top-color: #e6e6e6\">&nbsp;</div>\n" +
                "                            <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"87.9%\" align=\"center\" style=\"margin: 0 auto; max-width: 87.9%; min-width: 87.9%;\">\n" +
                "                                <tbody><tr>\n" +
                "                                    <td>\n" +
                "                                        <div style=\"height: 30px; line-height: 50px; font-size: 48px\">&nbsp;</div>\n" +
                "                                        <div align=\"center\">\n" +
                "                                            <span style=\"font-family: Arial, sans-serif; font-size: 28px; line-height: 38px; color: #191919;\">Уважаемый " + name + "!<br>Спасибо Вам за участие в жизни нашего театра!</span>\n" +
                "                                            <div style=\"height: 20px; line-height: 20px; font-size: 18px\">&nbsp;</div>\n" +
                "                                            <span style=\"font-family: Arial, sans-serif; font-size: 24px; line-height: 30px; color: #999999;\">Ваша помощь, очень важна для нас и поможет воплотить в жизнь  творческие замыслы, выпустить новые спектакли." +
                "                                               <br>Обещаем, что Ваше внимание к нам сейчас - вернется добром ко всем таганрожцам. <br> </span>" +
                "                                               <div style=\"height: 20px; line-height: 20px; font-size: 18px\">&nbsp;</div>" +
                "                                               <span style=\"font-family: Arial, sans-serif; font-size: 28px; line-height: 38px; color: #191919;\">С благодарностью, команда таганрогского камерного театра.</span>\n" +
                "                                        </div>\n" +
                "                                        <div style=\"height: 35px; line-height: 35px; font-size: 33px\">&nbsp;</div>\n" +
                "                                        <div align=\"center\">\n" +
                "                                            <!--[if mso]>\n" +
                "                                            <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\"http://\" style=\"height: 68px; v-text-anchor: middle; width: 255px;\" strokecolor=\"#303030\" fillcolor=\"#303030\">\n" +
                "                                            <w:anchorlock/>\n" +
                "                                            <center style=\"color:#ffffff; font-family: Arial, sans-serif; font-size: 17px;\">Перейти на сайт</center>\n" +
                "                                            </v:rect>\n" +
                "                                            <![endif]-->\n" +
                "                                            <a href=\"tagteatr.ru\" style=\"background-color: red; color: #ffffff; display: inline-block; font-family: Arial, sans-serif; font-size: 17px; line-height: 70px; text-align: center; text-decoration: none; width: 255px; -webkit-text-size-adjust: none; mso-hide: all; background: red; border-radius: 4px;\">Перейти на сайт</a>\n" +
                "                                        </div>\n" +
                "                                        \n" +
                "                                        \n" +
                "                                        <div style=\"height: 15px; line-height: 15px; font-size: 13px\">&nbsp;</div>\n" +
                "                                        \n" +
                "                                        <div style=\"height: 25px; line-height: 25px; font-size: 23px\">&nbsp;</div>\n" +
                "                                        <div style=\"height: 1px; line-height: 1px; font-size: 1px; border-top-width: 2px; border-top-style: solid; border-top-color: #e6e6e6\">&nbsp;</div>\n" +
                "                                        <div style=\"height: 45px; line-height: 45px; font-size: 43px\">&nbsp;</div>\n" +
                "                                        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" style=\"margin: 0 auto;\">\n" +
                "                                            <tbody><tr>\n" +
                "                                                <td width=\"27\">\n" +
                "                                                    <a href=\"https://vk.com/tagteatr\" target=\"_blank\"><img src=\"https://imgems.ru/ems/templates/vk.png\" width=\"27\" height=\"19\" border=\"0\" alt=\"\" style=\"display: block\"></a>\n" +
                "                                                </td>\n" +
                "                                                <td width=\"50\">&nbsp;</td>\n" +
                "                                                <td width=\"27\">\n" +
                "                                                    <a href=\"https://www.instagram.com/tagteatr/\" target=\"_blank\"><img src=\"https://imgems.ru/ems/templates/ig.png\" width=\"27\" height=\"19\" border=\"0\" alt=\"\" style=\"display: block\"></a>\n" +
                "                                                </td>\n" +
                "                                                <td width=\"50\">&nbsp;</td>\n" +
                "                                                <td width=\"27\">\n" +
                "                                                    <a href=\"https://www.facebook.com/tagteatr/\" target=\"_blank\"><img src=\"https://imgems.ru/ems/templates/fb.png\" width=\"27\" height=\"19\" border=\"0\" alt=\"\" style=\"display: block\"></a>\n" +
                "                                                </td>\n" +
                "                                                \n" +
                "                                                \n" +
                "                                            </tr>\n" +
                "                                        </tbody></table>\n" +
                "                                        <div style=\"height: 35px; line-height: 35px; font-size: 33px\">&nbsp;</div>\n" +
                "                                        <div align=\"center\">\n" +
                "                                            <span style=\"font-family: Arial, sans-serif; font-size: 13px; line-height: 18px; color: #999999;\">Если вы получили это письмо по ошибке, проигнорируйте его.</span>\n" +
                "                                            <div style=\"height: 30px; line-height: 30px; font-size: 28px\">&nbsp;</div>\n" +
                "                                            \n" +
                "                                        </div>\n" +
                "                                        <div style=\"height: 45px; line-height: 45px; font-size: 43px\">&nbsp;</div>\n" +
                "                                    </td>\n" +
                "                                </tr>\n" +
                "                            </tbody></table>\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                </tbody></table>\n" +
                "                <!--[if (gte mso 9)|(IE)]>\n" +
                "                </td></tr></table>\n" +
                "                <![endif]-->\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "    </tbody></table>\n" +
                "\n" +
                "\n" +
                "<!-- partial -->\n" +
                "  \n" +
                "\n" +
                "\n" +
                "</body></html>"
            };

            sendEmail(msg, function (err) {
                if(err){
                    console.log('^^^^^@@@@@', err);
                    res.send('Ошибка при отправке сообщения');
                } else{
                    res.send('Письмо отправлено!');
                }
            });
        } else {
            res.send('Ошибка при отправке сообщения');
        }
    });

    app.get('/payment', function (req, res) {
        console.log('&&&&&', req.body);
        res.send(200);
    });

    app.post('/yandex-payment', function (req, res) {
        if (req.body) {
            var headers = {
                'Idempotence-Key': uuid.v4(),
                'Content-Type': 'application/json'
            };
            var dataString = {
                amount: {
                    value: req.body.amount,
                    currency: "RUB"
                },
                confirmation: {
                    type: "embedded"
                },
                capture: true,
                description: "Пожертвование на уставную деятельность АНО «ТаКТ»"
            };
            function callback(error, response, body) {
                console.log('^^^^^', error, body);
                if (!error && response.statusCode === 200) {
                    var data = JSON.parse(body);
                    res.send({status: 'success', confirmation_token: data && data.confirmation && data.confirmation.confirmation_token} );
                } else {
                    res.send({status: 'error', data: error});
                }
            }
            request.post({
                url: 'https://payment.yandex.net/api/v3/payments',
                headers: headers,
                body: JSON.stringify(dataString),
                auth: {
                    'user': process.env.YANDEX_API_ID,
                    'pass': process.env.YANDEX_API_KEY
                }
            }, callback);
        } else {
            res.send('ошибка оплаты');
        }
    });

};


