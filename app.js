var express = require('express')
    , http = require('http')
    , routes = require('./routes')
    , nm = require('express-namespace')
    , env       = process.env.NODE_ENV || "development"
    , config    = require('./config/config')[env];

var app = express();

var server = http.createServer(app);

require('./config/express')(app, express, config);

server.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
