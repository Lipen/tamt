var path = require('path');

module.exports = function(grunt) {
    // Do grunt-related things in here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        cssmin: {
            options: {
                rebase: true,
                rebaseTo: path.resolve('build/css')
            },
            target: {
                files: [{
                    src: 'public/stylesheets/*.css',
                    dest: 'public/build/css/style.min.css'
                }]
            }
        },
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            app: {
                files: {
                    './public/build/js/appDirectives.js': ['./public/javascripts/directives/directives.js'],
                    './public/build/js/appServices.js': ['./public/javascripts/services/service.js'],
                    './public/build/js/Controllers.js': ['./public/javascripts/controllers/*.js', '!./public/javascripts/controllers/config.js']
                }
            }
        },
        concat: {
            js: { //target
                src: ['./public/build/app.js', './public/build/js/*.js'],
                dest: './public/build/min/app.js'
            }
        },
        uglify: {
            js: { //target
                src: ['./public/build/min/app.js'],
                dest: './public/build/min/app.js'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default', ['ngAnnotate', 'concat', 'uglify', 'cssmin']);
};