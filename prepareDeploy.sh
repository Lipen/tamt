#!/bin/bash
#ssh -i ~/.ssh/id_rsa root@159.65.124.146 < deployTest.sh;
#!/bin/sh
FILE="deploy.sh"

cat <<END > $FILE
#!/bin/sh

docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME
docker rmi $DOCKER_REPO
#---------------- Run
docker login -u $DOCKER_LOGIN -p $DOCKER_PASSWORD
docker pull imalakhov/tamt-img
docker run -it \
    -d \
    --restart=always \
    --name $CONTAINER_NAME \
    -p 3000:3000\
    $DOCKER_REPO
END

chmod 755 $FILE