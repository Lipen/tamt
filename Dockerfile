FROM node:8
WORKDIR /app
COPY package*.json ./
COPY . .
EXPOSE 3000
CMD [ "npm", "start" ]
