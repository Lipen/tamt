"use strict";

module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define("User", {
        name: {
            type: DataTypes.STRING,
            notNull: true
        },
        phone: {
            type: DataTypes.INTEGER
        },
        family: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.STRING
        },
        emailrecovery: {
            type: DataTypes.STRING
        },
        check: {
            type: DataTypes.BOOLEAN
        },
        password: {
            type: DataTypes.STRING
        },
        avatar: {
            type: DataTypes.STRING
        }
    }, {
        tableName: 'User',
        classMethods: {
            associate: function(models) {
            }
        }
    });

    return User;
};