"use strict";

var fs        = require("fs");
var path      = require("path");
var Sequelize = require("sequelize");
var env       = process.env.NODE_ENV || "development";
var config    = require(__dirname + '/../config/config.json')[env];
var sequelize = new Sequelize(config.database, config.username, config.password, config);
var db        = {};
fs
    .readdirSync(__dirname + '/models')
    .forEach(function(file) {
        var model = sequelize["import"](path.join(__dirname+ '/models', file));
        db[model.name] = model;
    });

Object.keys(db).forEach(function(modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});
//sequelize.sync({force:true});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;