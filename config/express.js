var  path = require('path');

module.exports = function (app, express, config) {
    app.set('port', process.env.PORT || 3000);
    app.set('views', path.join(__dirname, '..', 'views'));
    app.set('view engine', 'ejs');
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser());
    app.use(express.session(config.sessionOptions));

    app.use(app.router);
    app.use(express.static(path.join(__dirname, '..', 'public')));
    app.set('db', require('../db/index'));

    if ('development' == app.get('env')) {
        app.use(express.errorHandler());
    }

    app.get('*.*', express.static(path.join(__dirname, '..', 'public')));

    app.get('/.well-known/apple-developer-merchantid-domain-association', function(req, res) {
        res.sendfile(path.resolve('.well-known/apple-developer-merchantid-domain-association'));
    });

    app.namespace("/", require('../routes/index')
        .boot.bind(this, app, config));

    app.namespace("/user", require('../routes/user')
        .boot.bind(this, app, config));
};
